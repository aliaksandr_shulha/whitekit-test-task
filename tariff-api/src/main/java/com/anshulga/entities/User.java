package com.anshulga.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class User {
    @Id
    private Long id;
    private String name;
    private long balance;
    @ManyToOne
    private Tariff tariff;
}
