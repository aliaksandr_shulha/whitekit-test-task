<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="List">
        <Tariff>
            <xsl:apply-templates/>
        </Tariff>
    </xsl:template>
    <xsl:template match="/">
        <xsl:element name="List">
            <xsl:for-each select="List/item">
                <xsl:element name="clientInfo">
                    <xsl:value-of select="id"/>/<xsl:value-of select="balance"/>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>