package com.anhsulga.cacheservice.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

@Document
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserInfo {
    @Id
    private String clientId;
    @Field
    private Integer balance;

    public UserInfo(String str) {
        String[] temp = str.split("/");
        clientId = temp[0];
        balance = Integer.valueOf(temp[1]);
    }
}
