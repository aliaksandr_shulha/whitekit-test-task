package com.anhsulga.cacheservice.repository;

import com.anhsulga.cacheservice.entities.UserInfo;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BalanceRepository extends CouchbaseRepository<UserInfo,String> {
    List<UserInfo> getByBalanceLessThan(Integer i);
}
