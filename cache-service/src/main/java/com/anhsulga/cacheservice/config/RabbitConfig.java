package com.anhsulga.cacheservice.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    @Value("${direct.exchange}")
    private String exchange;
    @Value("${couchbase.queue}")
    private String queue;
    @Value("${direct.couchbase.routing}")
    private String routingKey;

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    public Binding couchbaseBinding(DirectExchange directExchange, Queue couchbaseQueue) {
        return BindingBuilder.bind(couchbaseQueue).to(directExchange).with(routingKey);
    }

    @Bean
    public Queue couchbaseQueue() {
        return new Queue(queue);
    }
}
