package com.anhsulga.cacheservice.service;

import com.anhsulga.cacheservice.entities.UserInfo;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

@Service
@Slf4j
public class XmlParsingService {
    @Value("${xslt.schema}")
    private String schemaName;
    private final ResourceLoader resourceLoader;
    private final CacheService cacheService;
    public XmlParsingService(ResourceLoader resourceLoader, CacheService cacheService) {
        this.resourceLoader = resourceLoader;
        this.cacheService = cacheService;
    }

    @SneakyThrows
    public Source convertXml(String inputXml) {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(resourceLoader.getResource(schemaName).getInputStream());
        Transformer transformer = factory.newTransformer(xslt);
        Source xml = new StreamSource(new StringReader(inputXml));
        StringWriter writer = new StringWriter();
        Result result = new StreamResult(writer);
        transformer.transform(xml, result);
        return new StreamSource(new StringReader(writer.toString()));
    }

    @SneakyThrows
    public void parseXmlAndSaveToCache(Source xml) {
        XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(xml);
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == XMLEvent.START_ELEMENT && "clientInfo".equals(reader.getLocalName())) {
                String clientInfo = reader.getElementText();
                cacheService.save(new UserInfo(clientInfo));
            }
        }
    }
}
