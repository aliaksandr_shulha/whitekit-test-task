package com.anhsulga.cacheservice.service;

import com.anhsulga.cacheservice.entities.UserInfo;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.xml.transform.Source;
import java.util.List;

@Service
public class ScheduledService {
    @Value("${scheduler.cacheRate}")
    private long cacheRate;
    @Value("${scheduler.amqpRate}")
    private long amqpRate;
    @Value("${amqp.min}")
    private int minBalance;
    @Value("${direct.exchange}")
    private String exchange;
    @Value("${couchbase.queue}")
    private String queue;
    @Value("${direct.couchbase.routing}")
    private String routingKey;
    private final ThreadPoolTaskScheduler scheduler;
    private final UserInfoService userInfoService;
    private final XmlParsingService xmlParsingService;
    private final RabbitTemplate rabbitTemplate;
    private final CacheService cacheService;

    public ScheduledService(ThreadPoolTaskScheduler threadPoolTaskScheduler,
                            UserInfoService userInfoService,
                            XmlParsingService xmlParsingService,
                            RabbitTemplate rabbitTemplate,
                            CacheService cacheService) {
        this.scheduler = threadPoolTaskScheduler;
        this.userInfoService = userInfoService;
        this.xmlParsingService = xmlParsingService;
        this.rabbitTemplate = rabbitTemplate;
        this.cacheService = cacheService;
    }

    @PostConstruct
    public void scheduleUsersInfo() {
        scheduler.scheduleAtFixedRate(this::cacheTask, cacheRate);
        scheduler.scheduleAtFixedRate(this::amqpTask, amqpRate);
    }

    private void cacheTask() {
        String xml = userInfoService.getXmlInfo();
        Source source = xmlParsingService.convertXml(xml);
        xmlParsingService.parseXmlAndSaveToCache(source);
    }

    private void amqpTask() {
        List<UserInfo> usersBalance = cacheService.getUsersBalance(minBalance);
        rabbitTemplate.convertAndSend(exchange, routingKey, usersBalance.toString().getBytes());
    }
}
