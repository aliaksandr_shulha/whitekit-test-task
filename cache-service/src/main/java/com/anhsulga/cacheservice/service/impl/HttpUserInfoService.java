package com.anhsulga.cacheservice.service.impl;

import com.anhsulga.cacheservice.service.UserInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpUserInfoService implements UserInfoService {
    @Value("${tariff.api.url}")
    private String apiUrl;
    @Override
    public String getXmlInfo() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> result = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
        return result.getBody();
    }
}
