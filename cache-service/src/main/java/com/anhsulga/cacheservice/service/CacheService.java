package com.anhsulga.cacheservice.service;

import com.anhsulga.cacheservice.entities.UserInfo;
import com.anhsulga.cacheservice.repository.BalanceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CacheService {
    private final BalanceRepository balanceRepository;

    public CacheService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public void save(UserInfo rate) {
        balanceRepository.save(rate);
    }

    public List<UserInfo> getUsersBalance(Integer integer) {
        return balanceRepository.getByBalanceLessThan(integer);
    }
}
